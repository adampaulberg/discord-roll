const discord = require("discord.js");
const roll = require('./lib/roll');

const config = require('./config');

let bot = new discord.Client({autoReconnect:true});

bot.on('ready', async () => {
    try{
        await roll.init(bot);
    } catch (err) {
        console.error(err);
    }
});

bot.on('message', async (message) => {
    await roll.handleMessage(bot, message);
});

bot.on('disconnected', () => {
    console.log('disconnected from discord');
});

bot.login(config.discordToken);