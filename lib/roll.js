const ChanceLib = require('chance');
const fs = require('fs');
const join = require('path').join;

let version = 'dev'

async function init(bot) {
    let botClient = bot.user;

    botClient.setStatus('online');
    botClient.setUsername('Gai');

    let filepath = join(__dirname, '../version.txt');
    let githash = fs.readFileSync(filepath, 'utf8');
    version = githash.trim();
}

async function handleMessage(bot, message) {
    let isMentioned = false;
    let chance = new ChanceLib();

    //check for mentions
    let mentions = [];
    message.mentions.users.forEach(function(mention) {
        if(mention.id === bot.user.id)  {
            isMentioned = true;
        } else {
            mentions.push(mention);
        }
    });

    let params = message.content.split(' ');
    let whom = params.shift();

    if(!isMentioned && whom && (whom.toLowerCase() == '!gai' || whom.toLowerCase() == '!gaipad')) {
        isMentioned = true;
    }

    if(isMentioned) {
        let response = false;
        let command = params.shift();

        if(/roll/i.test(command)){
            let dieType = params && params.length && params[0];

            //make sure in the correct format
            if(/[0-9]?.d[0-9]+$/i.test(dieType)){
                let result = chance.rpg(dieType.toLowerCase());
                let sum = result.reduce(function(a, b) { return a + b; });

                response = '```Rolled: ' + result.join(', ') + '```';
                
                let dieArr = dieType.split("d");
                if(dieArr.length > 1 && dieArr[0] > 1) {
                    response += ' ```Sum: ' + sum + '```';
                }
                
            }
        }
        else if (/version/i.test(command)) {
            response = 'I am running on commit ' + version
        }

        else if (/gaiflix|movies/i.test(command)) {
            response = 'For your viewing pleasure, we currently have: ```One Hour Photo```'
        }

        if(response) message.channel.send(response);
    }

}

module.exports = {
    init: init,
    handleMessage: handleMessage
}